package com.android.learn.backendlesstesting.utility;

import android.content.Context;

import com.android.learn.backendlesstesting.database.backendless.JsonObjectRequestBackendless;
import com.android.learn.backendlesstesting.model.Address;
import com.android.learn.backendlesstesting.model.UserAccount;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by jame_h on 10/10/2016.
 */

public class HttpApiClient {

    private static HttpApiClient sSharedInstance;
    private static RequestQueue mRequestQueue;

    private HttpApiClient()
    {

    }

    public static synchronized HttpApiClient getSharedIntance(Context context)
    {
        if (sSharedInstance == null) {
            sSharedInstance = new HttpApiClient();
            mRequestQueue = Volley.newRequestQueue(context);
        }

        return sSharedInstance;
    }

    public void getUserAccounts(final OnUserAccountsLoadedListener handler)
    {
        String url = "https://api.backendless.com/v1/data/UserAccounts?relationsDepth=1";

        JsonObjectRequest request = new JsonObjectRequestBackendless(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        handler.onUserAccountLoadedComplete(response, null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        handler.onUserAccountLoadedComplete(null, error);
                    }
                });

        mRequestQueue.add(request);
    }

    public void addNewUserAccount(JSONObject body, final OnAddNewUserAccountListener handler)
    {
        String url = "https://api.backendless.com/v1/data/UserAccounts";

        JsonObjectRequest request = new JsonObjectRequestBackendless(Request.Method.POST, url, body,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        handler.onAddNewAccountComplete(response, null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        handler.onAddNewAccountComplete(null, error);
                    }
                });

        mRequestQueue.add(request);
    }

    public void deleteAddress(String objectId, final OnDeleteAddressListener handler)
    {
        String url = "https://api.backendless.com/v1/data/Addresses/" + objectId;

        JsonObjectRequest request = new JsonObjectRequestBackendless(Request.Method.DELETE, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        handler.OnDeleteAddressComplete(response, null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        handler.OnDeleteAddressComplete(null, error);
                    }
                });

        mRequestQueue.add(request);
    }


    public interface OnUserAccountsLoadedListener {
        public void onUserAccountLoadedComplete(JSONObject response, VolleyError error);
    }

    public interface OnAddNewUserAccountListener {
        public void onAddNewAccountComplete(JSONObject response, VolleyError error);
    }

    public interface OnDeleteAddressListener {
        public void OnDeleteAddressComplete(JSONObject response, VolleyError error);
    }


}
