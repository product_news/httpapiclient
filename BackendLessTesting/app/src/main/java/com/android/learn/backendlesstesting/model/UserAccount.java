package com.android.learn.backendlesstesting.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jame_h on 10/10/2016.
 */

public class UserAccount {
    private static final String TAG = "UserAccount";
    String mEmail;
    Address mAddress;
    String  mObjectId;

    public UserAccount(String email, Address address)
    {
        mEmail = email;
        mAddress = address;
    }

    public UserAccount()
    {
    }

    public UserAccount toModel(JSONObject jsonObject)
    {
        UserAccount userAccount = new UserAccount();

        try {
            userAccount.mEmail = (String) jsonObject.get("Email");
            JSONArray jsonObjects = jsonObject.getJSONArray("Address");

            for (int i = 0; i < jsonObjects.length(); i++) {
                JSONObject obj = jsonObjects.getJSONObject(i);

                userAccount.mAddress.mStreet = obj.getString("Street");
                userAccount.mAddress.mCity = obj.getString("City");
            }

        } catch (JSONException e) {

            Log.d(TAG, "toModel: ");
            e.printStackTrace();
        }

        return userAccount;
    }

    public JSONObject toJson()
    {
        JSONObject obj = new JSONObject();

        try {
            obj.put("Email", this.mEmail);
            obj.put("Address", this.mAddress.toJasonArray());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj;
    }

    public String getEmail()
    {
        return mEmail;
    }

    public void setEmail(String email)
    {
        mEmail = email;
    }

    public Address getAddress()
    {
        return mAddress;
    }

    public void setAddress(Address address)
    {
        mAddress = address;
    }
}
