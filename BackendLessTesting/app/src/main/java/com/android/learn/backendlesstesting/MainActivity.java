package com.android.learn.backendlesstesting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.learn.backendlesstesting.model.Address;
import com.android.learn.backendlesstesting.model.UserAccount;
import com.android.learn.backendlesstesting.utility.HttpApiClient;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements HttpApiClient.OnUserAccountsLoadedListener, HttpApiClient.OnAddNewUserAccountListener, HttpApiClient.OnDeleteAddressListener {

    private static final String TAG = "MainActivity";
    private TextView mTextViewResult;
    private Button mButtonGo;
    private HttpApiClient mHttpApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextViewResult = (TextView) findViewById(R.id.activity_main_text_view);
    }


    @Override
    protected void onResume()
    {
        super.onResume();

        mHttpApiClient = HttpApiClient.getSharedIntance(getApplicationContext());


        Address newAddress = new Address("Jalan  Duta", "Puchong", 0, "", "");
        UserAccount newUser = new UserAccount("karen@example.com", newAddress);

        Log.d(TAG, "onResume: " + newUser.toJson());

        mHttpApiClient.addNewUserAccount(newUser.toJson(), this);
        mHttpApiClient.getUserAccounts(this);
//        httpApiClient.deleteAddress( "EA3BBC2A-8BCB-BDBC-FF30-9BFFA5602600", this);

    }

    @Override
    public void onUserAccountLoadedComplete(JSONObject response, VolleyError error)
    {
        try {
            if (response != null) {
                JSONArray data = response.getJSONArray("data");

                for (int i = 0; i < data.length(); i++) {
                    JSONObject jsonObject = (JSONObject) data.get(i);
                    Log.d(TAG, "Email: " + jsonObject.getString("Email"));
                    String email = jsonObject.getString("Password");

                    JSONArray addresses = jsonObject.optJSONArray("Address");
                    if (addresses != null) {
                        for (int j = 0; j < addresses.length(); j++) {
                            JSONObject address = (JSONObject) addresses.get(j);
                            Log.d(TAG, "Street: " + address.getString("Street"));
                            Log.d(TAG, "City: " + address.getString("City"));

                            String addressObjectId = address.getString("objectId");

                           // mHttpApiClient.deleteAddress( addressObjectId, this);
                        }
                    }

                    email += mTextViewResult.getText().toString() + "\n";
                    mTextViewResult.setText(email);

                }
            }

            if( error != null )
            {
                Log.d(TAG, "onUserAccountLoadedComplete: " + error.toString() );
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAddNewAccountComplete(JSONObject response, VolleyError error)
    {
        if (response != null) {
            Log.d(TAG, "onAddNewAccountComplete: " );
        }
        else
        {
            Log.d(TAG, "onAddNewAccountComplete: " + error.toString());
        }
    }

    @Override
    public void OnDeleteAddressComplete(JSONObject response, VolleyError error)
    {
        if (response != null) {
            Log.d(TAG, "OnDeleteAddressComplete: " );
        }
        else
        {
            Log.d(TAG, "OnDeleteAddressComplete: " + error.toString());
        }
    }
}
