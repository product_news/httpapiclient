package com.android.learn.backendlesstesting.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by jame_h on 10/10/2016.
 */

public class Address {

    String mStreet;
    String mCity;
    int mZipCode;
    String mState;
    String mCountry;
    String mObjectId;

    public Address(String street, String city, int zipCode, String state, String country)
    {
        mStreet = street;
        mCity = city;
        mZipCode = zipCode;
        mState = state;
        mCountry = country;
    }

    public String getStreet()
    {
        return mStreet;
    }

    public void setStreet(String street)
    {
        mStreet = street;
    }

    public String getCity()
    {
        return mCity;
    }

    public void setCity(String city)
    {
        mCity = city;
    }

    public int getZipCode()
    {
        return mZipCode;
    }

    public void setZipCode(int zipCode)
    {
        mZipCode = zipCode;
    }

    public String getState()
    {
        return mState;
    }

    public void setState(String state)
    {
        mState = state;
    }

    public String getCountry()
    {
        return mCountry;
    }

    public void setCountry(String country)
    {
        mCountry = country;
    }

    public JSONArray toJasonArray()
    {

        JSONObject jsonObject = new JSONObject();

        ArrayList<JSONObject> arrayList = new ArrayList<>();

        try {

            jsonObject.put("City", this.mCity);
            jsonObject.put("Street", this.mStreet);
            jsonObject.put("State", this.mState);
            jsonObject.put("___class", "Addresses");

            arrayList.add(jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONArray obj = new JSONArray(arrayList);

        return obj;
    }


    public JSONObject toJason()
    {

        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("City", this.mCity);
            jsonObject.put("Street", this.mStreet);
            jsonObject.put("State", this.mState);
            jsonObject.put("___class", "Addresses");



        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
}
